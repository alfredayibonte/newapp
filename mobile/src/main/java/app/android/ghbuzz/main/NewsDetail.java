package app.android.ghbuzz.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.util.HashMap;
import java.util.List;

import app.android.ghbuzz.R;
import app.android.ghbuzz.adapter.NewsItem;
import app.android.ghbuzz.util.TextSliderView;
import app.android.ghbuzz.util.Util;

public class NewsDetail extends ActionBarActivity implements BaseSliderView.OnSliderClickListener {
    private Intent intent;
    private SliderLayout slider;
    private Context context;
    private ActionBar actionBar;
    private ImageView imageViewNumber, imageView;
    private HashMap<String, String> url_maps;
    private TextView textViewArtistOtherStories, textViewCategory, textViewNewsContent, textViewNewsTitle, textViewNumber;
    private String defaultImage = "";
    public static NewsItem list = new NewsItem("", "", "", URI.create(""), "", "" , "", "", "");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        intent = getIntent();
        context = this;
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        int color = Util.randInt(0, 5);
        defaultImage = list.getImageUrl().toString();
        imageViewNumber = (ImageView) findViewById(R.id.imageViewNumber);
        textViewNewsTitle = (TextView)findViewById(R.id.textViewNewsTitle);
        imageView = (ImageView) findViewById(R.id.imageView);
        textViewArtistOtherStories = (TextView) findViewById(R.id.textViewArtistOtherStories);
        textViewCategory = (TextView) findViewById(R.id.textViewCategory);
        textViewCategory.setText(list.getCategory());
        textViewNumber = (TextView)findViewById(R.id.textViewNumber);
        textViewNewsContent = (TextView)findViewById(R.id.textViewNewsContent);
        textViewNewsContent.setText(list.getContent());
        textViewNewsTitle.setText(list.getTitle());
        slider = (SliderLayout) findViewById(R.id.slider);
        slider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));
        imageViewNumber.setImageResource(R.color.green);


        url_maps = new HashMap<String, String>();
        Picasso.with(context)
                .load(defaultImage)
                .centerCrop()
                .error(color)
                .placeholder(color)
                .resize(200, 300)
                .into(imageView);
        url_maps.put("1" + "<ghbuzz>" + "First media", defaultImage);
        url_maps.put("2" + "<ghbuzz>" + "Second media", defaultImage);
        addRelatedMedia();

    }

    private void addRelatedMedia() {
        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(context);
            String splitter = "<ghbuzz>";
            String idOfEvent = name.split(splitter)[0];
            String nameOfEvent = name.split(splitter)[1];
            textSliderView
                    .description(nameOfEvent)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

            textSliderView.getBundle().putString("extra", nameOfEvent);
            textSliderView.getBundle().putString("id", idOfEvent);


            slider.addSlider(textSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(4000);

        textViewArtistOtherStories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.setClass(context, RelatedNewsActivity.class);
                intent.putExtra("category", textViewCategory.getText().toString());
                startActivity(intent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {

    }
}
