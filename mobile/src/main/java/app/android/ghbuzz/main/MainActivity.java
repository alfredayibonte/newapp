package app.android.ghbuzz.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nhaarman.listviewanimations.appearance.simple.SwingLeftInAnimationAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import app.android.ghbuzz.R;
import app.android.ghbuzz.adapter.NewsItem;
import app.android.ghbuzz.adapter.NewsItemAdapter;
import app.android.ghbuzz.persistence.App;
import app.android.ghbuzz.util.Util;

public class MainActivity extends ActionBarActivity {
    private static final int MY_SOCKET_TIMEOUT_MS = 30000;
    private ActionBar actionBar;
    private LinearLayout linearLayoutLoading;
    private ListView list_item;
    private NewsItemAdapter adapter;
    private List<NewsItem> list;
    private Context context;
    private Intent intent;
    private String defaultImage = "";
    SwingLeftInAnimationAdapter animationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        View footer = getLayoutInflater().inflate(R.layout.news_feed_footer, null);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);

        List<App> app = App.listAll(App.class);
        if (app.isEmpty()) {
            App appFirst = new App(true);
            appFirst.save();
        }


        Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
        int currentDay = localCalendar.get(Calendar.DATE);
        int currentMonth = localCalendar.get(Calendar.MONTH) + 1;


        String mon;
        switch (currentMonth){
            case 1:
                mon = "JANUARY";
                break;
            case 2:
                mon = "FEBRUARY";
                break;
            case 3:
                mon = "MARCH";
                break;
            case 4:
                mon = "APRIL";
                break;
            case 5:
                mon = "MAY";
                break;
            case 6:
                mon = "JUNE";
                break;
            case 7:
                mon = "JULY";
                break;
            case 8:
                mon = "AUGUST";
                break;
            case 9:
                mon = "SEPTEMBER";
                break;
            case 10:
                mon = "OCTOBER";
                break;
            case 11:
                mon = "NOVEMBER";
                break;
            case 12:
                mon = "DECEMBER";
                break;
            default:
                mon = "TODAY";
                break;



        }



        String displayDate = mon + ", " + currentDay;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(displayDate);
        if (Util.getTime() >= 19) {
            relativeLayout.setBackgroundResource(R.color.dark);
        } else {
            relativeLayout.setBackgroundResource(R.color.white);
            textView.setTextColor(getResources().getColor(R.color.orange));
        }


        context = this;
        intent = new Intent();
        list_item = (ListView) findViewById(R.id.list_item);
        list_item.addFooterView(footer);
        linearLayoutLoading = (LinearLayout) findViewById(R.id.linearLayoutLoading);
        list = new ArrayList<NewsItem>();
        adapter = new NewsItemAdapter(context, R.layout.new_item_layout, list);
        RequestQueue queue = Volley.newRequestQueue(this);

        JsonArrayRequest myReq = new JsonArrayRequest(
                "http://www.sproutmultimedia.com/information.json",

              createMyReqSuccessListener(),
                createMyReqErrorListener()
        );
        JsonArrayRequest myReq2 = new JsonArrayRequest(
                "http://www.sproutmultimedia.com/sports_news.json",

                createMyReqSuccessListener(),
                createMyReqErrorListener()
        );
        myReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(myReq);
        queue.add(myReq2);
        queue.start();

        animationAdapter = new SwingLeftInAnimationAdapter(adapter);
        animationAdapter.setAbsListView(list_item);
        list_item.setAdapter(animationAdapter);
        linearLayoutLoading.setVisibility(View.GONE);

        list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NewsItem newsItem = (NewsItem)parent.getAdapter().getItem(position);
                NewsDetail.list = newsItem;


                if (position != adapter.getCount()) {
                    intent.setClass(context, NewsDetail.class);
                    startActivity(intent);
                }

            }
        });
        list_item.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (mLastFirstVisibleItem < firstVisibleItem) {
                    actionBar.hide();
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    actionBar.show();

                }
                mLastFirstVisibleItem = firstVisibleItem;
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()){
            case R.id.action_settings:
                intent.setClass(context, SettingActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_category:
                startActivity(new Intent(this, CategorySettingsActivity.class));
                return true;
            case R.id.action_share:
                new MaterialDialog.Builder(context)
                        .title("Share App With -:)")
                        .items(new String[]{"Apps"})
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            }
                        })
                        .show();
                return  true;

            case R.id.action_feedback:
                return true;
            case R.id.action_refresh:
                refresh();
                return true;
            case android.R.id.home:
                finish();
                return true;

        }




        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    private void refresh() {
        finish();
        startActivity(getIntent());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private Response.Listener<JSONArray> createMyReqSuccessListener() {
        return new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                String result =   response.toString();
                Log.e("result", result);
                try {
//                    NewsItem item = new NewsItem();
//                    item.setCategory();
                    JSONArray jsonArray = new JSONArray(result);
                    for(int i=0 ; i< jsonArray.length(); i++){
                        JSONObject object = jsonArray.getJSONObject(i);
                        String id =  object.getString("id");
                        String title = object.getString("title");
                        String content = object.getString("message");
                        String imageLink = object.getString("imageLink");
                        URI imageURL = URI.create(imageLink);
                        String webLink = object.getString("weblink");
                        String source = webLink;
                        String category =  object.getString("category");
                        String facebook = object.getString("facebookLink");
                        String twitter = object.getString("twitterLink");
                        NewsItem item = new NewsItem(id, title, source, imageURL, category, content , webLink, facebook, twitter);
                        list.add(item);
                    }
                   
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        };
    }
}



