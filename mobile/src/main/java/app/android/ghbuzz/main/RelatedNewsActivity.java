package app.android.ghbuzz.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nhaarman.listviewanimations.appearance.simple.SwingLeftInAnimationAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import app.android.ghbuzz.R;
import app.android.ghbuzz.adapter.NewsItem;
import app.android.ghbuzz.adapter.NewsItemAdapter;
import app.android.ghbuzz.adapter.RelatedNewsItemAdapter;
import app.android.ghbuzz.util.Util;

public class RelatedNewsActivity extends ActionBarActivity {
    private ActionBar actionBar;
    private LinearLayout linearLayoutLoading;
    private ListView list_item;
    private RelatedNewsItemAdapter adapter;
    private List<NewsItem> list;
    private Context context;
    private Intent intent;
    private String defaultImage = "http://static.businessinsider.com/image/53d26b63eab8eac12759b0aa-400/19-liz-levy-associate-creative-director-at-tbwachiatday-la.jpg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_related_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        setSupportActionBar(toolbar);
        intent = getIntent();
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText("Others in " + intent.getStringExtra("category"));
        if (Util.getTime() >= 19) {
            relativeLayout.setBackgroundResource(R.color.dark);
        } else {
            relativeLayout.setBackgroundResource(R.color.white);
            textView.setTextColor(getResources().getColor(R.color.orange));
        }

        context = this;
        intent = new Intent();
        list_item = (ListView) findViewById(R.id.list_item);
        linearLayoutLoading = (LinearLayout) findViewById(R.id.linearLayoutLoading);
        list = new ArrayList<NewsItem>();
        adapter = new RelatedNewsItemAdapter(context, R.layout.related_item_layout, list);

        list.add(new NewsItem("1", "CHRAJ workers up in arms against chief accountant", "My joyonline", URI.create("http://static.pulse.com.gh/img/incoming/crop3359113/5952789379-chorizontal-w644/1.jpg"), "Human right", "Content", "", "", ""));
        list.add(new NewsItem("2", "Obesity could be a disability", "Ghana web", URI.create("http://images.hngn.com/data/images/full/333/obesity-school-violence-and-gun-related-injury-among-top-10-child-health-concerns-in-us.jpg"), "Life style", "Content", "", "", ""));
        list.add(new NewsItem("3", "Make history, work to end power crisis – MPs urge Kwabena Donkor", "Ghana web", URI.create(defaultImage), "Politics", "Content", "", "", ""));
        list.add(new NewsItem("4", "Marriage is not on my mind now – Empress Njamah", "Ghana web", URI.create(defaultImage), "Marriage", "Content", "", "", ""));
        list.add(new NewsItem("5", "Gossip column: Benzema, Godin, Martinez, Sneijder, Sterling, Bent", "Ghana web", URI.create(defaultImage), "Entertainment", "Content", "", "", ""));
        SwingLeftInAnimationAdapter animationAdapter = new SwingLeftInAnimationAdapter(adapter);
        animationAdapter.setAbsListView(list_item);
        list_item.setAdapter(animationAdapter);
        linearLayoutLoading.setVisibility(View.GONE);

        list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != adapter.getCount()) {
                    intent.setClass(context, NewsDetail.class);
                    startActivity(intent);
                }
            }
        });

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
