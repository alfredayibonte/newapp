package app.android.ghbuzz.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialogCompat;
import com.daimajia.slider.library.SliderLayout;
import com.nhaarman.listviewanimations.appearance.simple.SwingLeftInAnimationAdapter;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import app.android.ghbuzz.R;
import app.android.ghbuzz.adapter.NewsItem;
import app.android.ghbuzz.adapter.NewsItemAdapter;
import app.android.ghbuzz.adapter.SettingAdapter;
import app.android.ghbuzz.adapter.SettingItem;
import app.android.ghbuzz.welcome.OnBoardActivity;

public class SettingActivity extends ActionBarActivity {
    private Intent intent;
    private Context context;
    private ActionBar actionBar;
    private ListView list_item;
    private SettingAdapter adapter;
    private List<SettingItem> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        intent = getIntent();
        context = this;
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");

        list_item = (ListView) findViewById(R.id.listView);
        list = new ArrayList<SettingItem>();
        adapter = new SettingAdapter(context, R.layout.new_item_layout, list);
        list.add(new SettingItem("Notification", "receive daily news"));
        list.add(new SettingItem("About " + getString(R.string.app_name), "info of app"));
        list.add(new SettingItem("Share app", "invite friends"));
        list.add(new SettingItem("Rate " + getString(R.string.app_name), "rate our app"));
        list.add(new SettingItem("How it works", "why use " + getString(R.string.app_name)));
        SwingLeftInAnimationAdapter animationAdapter = new SwingLeftInAnimationAdapter(adapter);
        animationAdapter.setAbsListView(list_item);
        list_item.setAdapter(animationAdapter);


        list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 4) {
                    intent.setClass(context, OnBoardActivity.class);
                    startActivity(intent);
                }
                if (position == 1) {
                    new MaterialDialog.Builder(context)
                            .title("About Afro Beatz")
                            .content("Let Google help apps determine location. This means sending anonymous location data to Google, even when no apps are running.")
                            .negativeText("Ok thanks")
                            .show();
                }
                if (position == 0) {
                    new MaterialDialog.Builder(context)
                            .title("Choose Favourite Time")
                            .items(new String[]{"5:00 am", "8:00 am", "9:00 am", "12:00 pm", "6:00 pm", "10:00 pm"})
                            .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                }
                            })
                            .positiveText("Choose")
                            .show();
                }


            }
        });
    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
