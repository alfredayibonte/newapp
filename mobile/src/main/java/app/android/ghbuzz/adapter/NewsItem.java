package app.android.ghbuzz.adapter;

import java.net.URI;


public class NewsItem {
    String id;
    String title;
    String source;
    String category;
    String content;
    String twitterLink;
    String facebookLink;
    String webLink;
    URI imageUrl;


    /**
     *
     * @param id
     * @param title
     * @param source
     * @param imageUrl
     * @param category
     * @param content
     * @param webLink
     * @param facebookLink
     * @param twitterLink
     */
    public NewsItem(String id, String title, String source, URI imageUrl, String category, String content,String webLink, String facebookLink,String twitterLink) {
        this.id = id;
        this.title = title;
        this.source = source;
        this.imageUrl = imageUrl;
        this.category = category;
        this.content = content;
        this.webLink = webLink;
        this.twitterLink = twitterLink;
        this.facebookLink = facebookLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }


    public URI getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(URI imageUrl) {
        this.imageUrl = imageUrl;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }
}
