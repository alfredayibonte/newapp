package app.android.ghbuzz.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import app.android.ghbuzz.R;
import app.android.ghbuzz.util.Util;

public class NewsItemAdapter extends ArrayAdapter<NewsItem> {

    Context context;
    private ViewHolder holder;


    public NewsItemAdapter(Context context, int resourceId,
                           List<NewsItem> items) {
        super(context, resourceId, items);
        this.context = context;

    }


    public View getView(int position, View convertView, ViewGroup parent) {
        holder = null;
        final NewsItem rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.new_item_layout, null);
            holder = new ViewHolder();
            holder.textViewNumber = (TextView) convertView.findViewById(R.id.textViewNumber);
            holder.textViewCategory = (TextView) convertView.findViewById(R.id.textViewCategory);
            holder.textViewNewsTitle = (TextView) convertView.findViewById(R.id.textViewNewsTitle);
            holder.textViewNewsSource = (TextView) convertView.findViewById(R.id.textViewNewsSource);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            holder.imageViewNumber = (ImageView) convertView.findViewById(R.id.imageViewNumber);
            holder.textViewNewsId = (TextView) convertView.findViewById(R.id.textViewNewsId);
            holder.textViewNewsImage = (TextView) convertView.findViewById(R.id.textViewNewsImage);
            holder.textViewNewsContent = (TextView) convertView.findViewById(R.id.textViewNewsContent);
            holder.linearLayoutLink = (LinearLayout) convertView.findViewById(R.id.linearLayoutLink);
            holder.linearLayoutMain = (LinearLayout) convertView.findViewById(R.id.linearLayoutMain);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.textViewNumber.setText(position + 1 + "");
        holder.textViewCategory.setText(rowItem.getCategory());
        holder.textViewNewsId.setText(rowItem.getId());
        holder.textViewNewsTitle.setText(rowItem.getTitle());
        holder.textViewNewsSource.setText(rowItem.getSource());
        holder.textViewNewsImage.setText(rowItem.getImageUrl().toString());
        holder.textViewNewsContent.setText(rowItem.getContent());

        if (Util.getTime() >= 19) {
            holder.linearLayoutMain.setBackgroundResource(R.color.dark);
        } else {
            holder.linearLayoutMain.setBackgroundResource(R.color.white);
            holder.textViewNewsTitle.setTextColor(getContext().getResources().getColor(R.color.dark));
            holder.textViewNewsSource.setTextColor(getContext().getResources().getColor(R.color.dark));
        }


        Picasso.with(getContext())
                .load(rowItem.getImageUrl().toString())
                .centerCrop()
                .error(R.color.white)
                .placeholder(R.color.pink)
                .resize(200, 300)
                .into(holder.imageView);

        if (position > 0) {
            holder.imageView.setVisibility(View.GONE);
        } else {
            holder.imageView.setVisibility(View.VISIBLE);
        }

        int color = Util.randInt(0, 9);


        holder.imageViewNumber.setImageResource(color);
        holder.textViewCategory.setTextColor(getContext().getResources().getColor(color));
        holder.linearLayoutLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .title("Related Links")
                        .items(new String[]{"Web Link", "Facebook", "Twitter"})
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                String link = "";
                                if (which == 0) {
                                    link = "";
                                } else if (which == 1) {
                                    link = "";
                                } else {
                                    link = "";
                                }
                                Util util = new Util(getContext());
                                util.connectWebLink(link);
                            }
                        })
                        .show();
            }
        });

        return convertView;
    }

    private class ViewHolder {
        ImageView imageView, imageViewNumber;
        TextView textViewNumber, textViewNewsTitle, textViewCategory;
        TextView textViewNewsSource, textViewNewsContent;
        TextView textViewNewsId, textViewNewsImage;
        LinearLayout linearLayoutLink, linearLayoutMain;
    }
}