package app.android.ghbuzz.adapter;


public class SettingItem {
    String resource;
    String displayText;
    String description;


    public SettingItem(String displayText, String description) {
        this.displayText = displayText;
        this.description = description;

    }


    public String getResource() {
        return resource;
    }

    public void setResource(String title) {
        this.resource = resource;
    }


    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
