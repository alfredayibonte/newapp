package app.android.ghbuzz.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.List;

import app.android.ghbuzz.R;

public class SettingAdapter extends ArrayAdapter<SettingItem> {

    Context context;
    private ViewHolder holder;


    public SettingAdapter(Context context, int resourceId,
                          List<SettingItem> items) {
        super(context, resourceId, items);
        this.context = context;

    }


    public View getView(int position, View convertView, ViewGroup parent) {
        holder = null;
        final SettingItem rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.setting_layout_item, null);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.textView);
            holder.textViewDescription = (TextView) convertView.findViewById(R.id.textViewDescription);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();


        holder.textView.setText(rowItem.getDisplayText());
        holder.textViewDescription.setText(rowItem.getDescription());


        return convertView;
    }


    private class ViewHolder {

        TextView textView;
        TextView textViewDescription;


    }
}