package app.android.ghbuzz.persistence;

import com.orm.SugarRecord;

/**
 * Created by bright on 12/24/14.
 */
public class News extends SugarRecord<News> {

    public String id;
    public String title;
    public String imageLink;
    public String message;
    public String weblink;
    public String facebookLink;
    public String twitterLink;
    public String youtubeLink;
    public String source;
    public String category;
    public boolean isRead;

    public News() {

    }
}
