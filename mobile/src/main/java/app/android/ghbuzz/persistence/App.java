package app.android.ghbuzz.persistence;


import com.orm.SugarRecord;

public class App extends SugarRecord<App> {

    public boolean installed;

    public App(boolean installed) {
        this.installed = installed;
    }

    public App() {
    }

}
