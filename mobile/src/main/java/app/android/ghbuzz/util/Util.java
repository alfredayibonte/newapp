package app.android.ghbuzz.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import app.android.ghbuzz.R;

/**
 * Created by bright on 12/24/14.
 */
public class Util {
    private Context context;

    public Util(Context context) {
        this.context = context;
    }

    public void connectWebLink(String link) {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            context.startActivity(browserIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        int color = R.color.pink;
        if (randomNum == 1) {
            color = R.color.pink;
        }
        if (randomNum == 2) {
            color = R.color.orange;
        }
        if (randomNum == 3) {
            color = R.color.red;
        }
        if (randomNum == 4) {
            color = R.color.blue;
        }
        if (randomNum == 5) {
            color = R.color.green;
        }
        if (randomNum == 6) {
            color = R.color.brown;
        }
        if (randomNum == 7) {
            color = R.color.facebook_blue;
        }
        if (randomNum == 8) {
            color = R.color.twitter_blue;
        }
        if (randomNum == 9) {
            color = R.color.deep_purple;
        }
        return color;
    }

    public static int getTime() {
        DateFormat dateFormat = new SimpleDateFormat("kk:mm:ss");
        Date date = new Date();
        String formattedDateTime = dateFormat.format(date).substring(0, 2);
        return Integer.parseInt(formattedDateTime);
    }
}
