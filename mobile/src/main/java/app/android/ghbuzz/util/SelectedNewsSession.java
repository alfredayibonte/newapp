package app.android.ghbuzz.util;

/**
 * Created by alfred on 8/6/14.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import app.android.ghbuzz.adapter.NewsItem;

public class SelectedNewsSession {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "newsPref";
    private static  final String NEWS_ID = "NEWS_ID";
    private static final String NEWS_LINK = "NEWS_LINK";




    /**
     * constructor for sessionManager
     * @param context
     */
    public SelectedNewsSession(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    /**
     *
     * @param newsItem
     */
    public void createSelectedNewsSession(NewsItem newsItem){
        // Storing login value as TRUE
        editor.putString(NEWS_ID, newsItem.getId());
        editor.putString(NEWS_LINK, newsItem.getWebLink());

        // commit changes
        editor.commit();
    }

    public String getSelectedNewsId(){



        return  pref.getString(NEWS_ID, "");
    }
    public String getSelectedNewLink(){
        return pref.getString(NEWS_LINK, "");
    }


}