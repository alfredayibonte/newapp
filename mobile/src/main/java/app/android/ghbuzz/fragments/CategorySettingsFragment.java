package app.android.ghbuzz.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import app.android.ghbuzz.R;

/**
 * Created by alfred on 2/6/15.
 */
public class CategorySettingsFragment  extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{
    //public static final String KEY_PREF_SYNC_CONN = "pref_key_storage_settings";
    SharedPreferences pref;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.category_settings);
         pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        pref.registerOnSharedPreferenceChangeListener(this);




    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        if (key.equals("pref_sport")) {
            Preference connectionPref = findPreference(key);
            boolean isSport = sharedPreferences.getBoolean(key, true);
            String message = isSport?  "Sports digest will be sent the next time": "You won't be served sports digest again";
            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        }
        else if(key.equals("pref_general")){
            boolean isGeneral = sharedPreferences.getBoolean(key, true);
            String message = isGeneral?  "General news digest will be sent the next time": "You won't be served General news digest again";
            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

        }
        else if(key.equals("pref_politics")){
            boolean isPolitics = sharedPreferences.getBoolean(key, true);
            String message = isPolitics?  "Politics digest will be sent the next time": "You won't be served Politics digest again";
            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        }
        else if(key.equals("pref_entertainment")){
            boolean isEntertainment = sharedPreferences.getBoolean(key, true);
            String message = isEntertainment?  "Entertainment digest will be sent the next time": "You won't be served  Entertainment digest again";
            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        }
    }




}
