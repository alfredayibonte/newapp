package app.android.ghbuzz.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import app.android.ghbuzz.R;
import app.android.ghbuzz.main.MainActivity;
import app.android.ghbuzz.util.Util;


public class AlarmReceiver extends BroadcastReceiver {
    private NotificationManager notificationManager;
    private static int NOTICE_ID = 1;
    private String title = "Afro Buzz ";
    private String body = "Hi, your morning buzz is ready";


    @Override
    public void onReceive(Context context, Intent intent) {

        if (Util.getTime() >= 19) {
            body = "Hi, your evening buzz is ready";
        }
        Intent notificationIntent = new Intent();
        notificationIntent.setClass(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher))
                        .setContentInfo(body)
                        .setContentTitle(title);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(body));
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);
        notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTICE_ID, builder.build());
    }
}