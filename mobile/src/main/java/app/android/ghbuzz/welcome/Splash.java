package app.android.ghbuzz.welcome;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import java.util.List;

import app.android.ghbuzz.R;
import app.android.ghbuzz.main.MainActivity;
import app.android.ghbuzz.persistence.App;


public class Splash extends Activity {
    private Intent intent;
    private CountDownTimer mCountDownTimer;
    private int i = 0;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        intent = new Intent();

        mCountDownTimer = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                i = i + 100;
            }

            @Override
            public void onFinish() {
                i++;
                List<App> app = App.listAll(App.class);
                if (app.isEmpty()) {
                    intent.setClass(context, OnBoardActivity.class);
                    startActivity(intent);
                } else {

                    intent.setClass(context, MainActivity.class);
                    startActivity(intent);

                }


            }
        };
        mCountDownTimer.start();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCountDownTimer.cancel();
        finish();
    }
}