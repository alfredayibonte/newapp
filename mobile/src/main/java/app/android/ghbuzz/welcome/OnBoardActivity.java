package app.android.ghbuzz.welcome;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import app.android.ghbuzz.R;
import app.android.ghbuzz.main.MainActivity;


public class OnBoardActivity extends Activity {
    private Context context;
    private ImageView imageViewOne, imageViewTwo, imageViewThree;

    private static final int MAX_VIEWS = 3;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_board);
        context = this;
        mViewPager = (ViewPager) findViewById(R.id.pager);
        imageViewOne = (ImageView) findViewById(R.id.imageViewOne);
        imageViewTwo = (ImageView) findViewById(R.id.imageViewTwo);
        imageViewThree = (ImageView) findViewById(R.id.imageViewThree);
        mViewPager.setAdapter(new OnBoardPagerAdapter());
        mViewPager.setOnPageChangeListener(new OnBoardPageChangeListener());
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        });


    }


    class OnBoardPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return MAX_VIEWS;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (View) object;
        }

        @Override
        public Object instantiateItem(View container, int position) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View imageViewContainer = inflater.inflate(R.layout.fragment_on_board, null);
            TextView textView = (TextView) imageViewContainer.findViewById(R.id.section_label);
            TextView textViewContent = (TextView) imageViewContainer.findViewById(R.id.section_label_content);
            YoYo.with(Techniques.SlideInLeft).delay(400).playOn(textView);
            YoYo.with(Techniques.SlideInRight).delay(400).playOn(textViewContent);

            switch (position) {
                case 0:
                    textView.setText(R.string.brief);
                    textViewContent.setText(R.string.why_brief);
                    break;

                case 1:
                    textView.setText(R.string.focus);
                    textViewContent.setText(R.string.why_focus);
                    break;

                case 2:
                    textView.setText(R.string.promise);
                    textViewContent.setText(R.string.why_promise);
                    break;

            }

            ((ViewPager) container).addView(imageViewContainer, 0);
            return imageViewContainer;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }


    class OnBoardPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {

                case MAX_VIEWS - 1:
                    imageViewOne.setImageResource(R.drawable.inactive_shape);
                    imageViewTwo.setImageResource(R.drawable.inactive_shape);
                    imageViewThree.setImageResource(R.drawable.active_shape);
                    break;
                case MAX_VIEWS - 2:
                    imageViewOne.setImageResource(R.drawable.inactive_shape);
                    imageViewTwo.setImageResource(R.drawable.active_shape);
                    imageViewThree.setImageResource(R.drawable.inactive_shape);
                    break;
                case MAX_VIEWS - 3:
                    imageViewOne.setImageResource(R.drawable.active_shape);
                    imageViewTwo.setImageResource(R.drawable.inactive_shape);
                    imageViewThree.setImageResource(R.drawable.inactive_shape);
                    break;


                default:

            }

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}